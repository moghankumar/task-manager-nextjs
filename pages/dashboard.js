import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import DashboardIcon from '@material-ui/icons/Dashboard';
import AssignmentIcon from '@material-ui/icons/Assignment';
import AutorenewIcon from '@material-ui/icons/Autorenew';
import AssignmentTurnedInIcon from '@material-ui/icons/AssignmentTurnedIn';
import MenuIcon from '@material-ui/icons/Menu';
import Dashboard from '../src/component/Dashboard';
import Head from 'next/head';
import { Nav, Navbar, NavDropdown } from 'react-bootstrap';
import Avatar from 'react-avatar';
import { userDetail } from '../src/services/user-services';
import { getCookie } from '../src/utils/cookie-helper';

function dashboard() {
  const router = useRouter();
  const [active, setActive] = useState(0);
  const [user, setUser] = useState('');

  useEffect(() => {
    // const data = router.asPath.split("=");
    // const email = data[1].split("%40");
    // const special = "@";
    // const fullemail = email[0].concat(special).concat(email[1]);
    // console.log(fullemail);
    usertask();
  }, []);
  function submit() {
    alert('Success');
  }
  const usertask = async () => {
    const id = getCookie().userid;

    const { data, status, error } = await userDetail(id);
    if (error) {
      toast.error('Unable to get user details', {
        position: toast.POSITION.BOTTOM_RIGHT,
        autoClose: false
      });
    } else if (status === 200 && data) {
      setUser(data.data);
    }
  };
  return (
    <>
      <Head>
        <title>Dashboard</title>
        <meta name="description" content="Dashboard" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div>
        <Navbar bg="dark" expand="lg" variant="light" className="text-white">
          <Navbar.Brand className="text-color font-weight-bold">
            Task Manager
          </Navbar.Brand>
          <Navbar.Toggle
            aria-controls="basic-navbar-nav"
            className="navbar-toggler text-color">
            <MenuIcon />
          </Navbar.Toggle>

          <Navbar.Collapse id="basic-navbar-nav" className="text-white">
            <Nav className="ms-auto text-white text-center ">
              <Nav.Link className="text-white">
                <Avatar
                  name={user.name}
                  size="35"
                  color={'#0070f3'}
                  textSizeRatio={1.75}
                  round={true}
                />
              </Nav.Link>

              <NavDropdown
                title={
                  <span className="text-white font-weight-bold">
                    {user.name}
                  </span>
                }
                id="basic-nav-dropdown"
                className="text-white text-color">
                <NavDropdown.Item onClick={() => router.push('/profile')}>
                  Profile
                </NavDropdown.Item>
                <NavDropdown.Item onClick={() => router.push('/')}>
                  Log Out
                </NavDropdown.Item>
              </NavDropdown>
            </Nav>
          </Navbar.Collapse>
        </Navbar>

        <div className="row">
          <div className="col-2">
            <div className="p-2 sidebar bg-dark">
              <nav className="navbar-dark">
                <ul className="navbar-nav text-white">
                  <li className="nav-item" onClick={() => setActive(0)}>
                    <span
                      className={
                        active === 0
                          ? 'nav-link active siderbar-row'
                          : 'nav-link siderbar-row'
                      }>
                      <span>
                        <DashboardIcon />
                      </span>
                      <span className="fw-bold ms-2 sidebar-title">
                        Dashboard
                      </span>
                    </span>
                  </li>
                  <li>
                    <hr />
                  </li>
                  <li className="nav-item" onClick={() => setActive(1)}>
                    <span
                      className={
                        active === 1
                          ? 'nav-link active siderbar-row'
                          : 'nav-link siderbar-row'
                      }>
                      <span>
                        <AssignmentIcon />
                      </span>
                      <span className="fw-bold ms-2 sidebar-title">Task</span>
                    </span>
                  </li>
                  <li className="nav-item" onClick={() => setActive(2)}>
                    <span
                      className={
                        active === 2
                          ? 'nav-link active siderbar-row'
                          : 'nav-link siderbar-row'
                      }>
                      <span>
                        <AutorenewIcon />
                      </span>
                      <span className="fw-bold ms-2 sidebar-title">
                        In Progress
                      </span>
                    </span>
                  </li>
                  <li className="nav-item" onClick={() => setActive(3)}>
                    <span
                      className={
                        active === 3
                          ? 'nav-link active siderbar-row'
                          : 'nav-link siderbar-row'
                      }>
                      <span>
                        <AssignmentTurnedInIcon />
                      </span>
                      <span className="fw-bold ms-2 sidebar-title">
                        Finished
                      </span>
                    </span>
                  </li>
                  {/* <li>
                      <hr />
                    </li> */}
                  {/* <li className="nav-item" onClick={() => setActive(4)}>
                      <span
                        className={
                          active === 4 ? "nav-link active" : "nav-link"
                        }
                      >
                        <span>
                          <AssignmentTurnedInIcon />
                        </span>
                        <span className="fw-bold ms-2">Graphs</span>
                      </span>
                    </li> */}
                </ul>
              </nav>
            </div>
          </div>

          <div className="col-10 mt-3 ">
            <div className="container">
              <Dashboard count={active} />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default dashboard;
