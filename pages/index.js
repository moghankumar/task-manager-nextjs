import Head from 'next/head';
import styles from '../styles/Home.module.css';
import React, { Component } from 'react';
import Link from 'next/link';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import Router from 'next/router';
import 'bootstrap/dist/css/bootstrap.css';
import { login } from '../src/services/auth-services';
import { saveCookie, getCookie } from '../src/utils/cookie-helper';

export class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      show: false
    };
  }

  submit(event) {
    event.preventDefault();
    if (this.state.email === '' && this.state.password === '') {
      toast.error('Email and password must needed', {
        position: toast.POSITION.BOTTOM_RIGHT,
        autoClose: false
      });
    } else if (!this.state.email.includes('@gmail.com')) {
      toast.warning('Invalid Email type', {
        position: toast.POSITION.BOTTOM_RIGHT,
        autoClose: false
      });
    } else {
      this.login();
    }
  }
  login = async () => {
    const userData = {
      email: this.state.email,
      password: this.state.password
    };
    const { data, status, error } = await login(userData);

    if (error) {
      toast.error('Login Failed...', {
        position: toast.POSITION.BOTTOM_RIGHT,
        autoClose: false
      });
    } else if (status === 200 && data) {
      // console.log('LOGIN:', data.user.id);

      saveCookie('token', data.authToken);
      saveCookie('userid', data.user.id);
      Router.push('/dashboard');
    }
  };
  render() {
    return (
      <div className="container">
        <Head>
          <title>Login</title>
          <meta name="description" content="Login your account" />
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <main className={styles.centerscreen}>
          <div id={styles.main} className="row">
            <div className="col-lg-6 col-md-6">
              <h3 className={styles.text_color}>
                Login and access your account
              </h3>
            </div>
            <div className="col-lg-6 col-md-6">
              <div className={styles.card}>
                <h2 className="text-center">Login</h2>
                <form onSubmit={this.submit.bind(this)}>
                  <div className="mb-3">
                    <label htmlFor="exampleInputEmail1" className="form-label">
                      Email address
                    </label>
                    <input
                      type="email"
                      className="form-control"
                      id="exampleInputEmail1"
                      aria-describedby="emailHelp"
                      onChange={(event) =>
                        this.setState({ email: event.target.value })
                      }
                    />
                    <div id="emailHelp" className="form-text">
                      We'll never share your email with anyone else.
                    </div>
                  </div>
                  <div className="mb-3">
                    <label
                      htmlFor="exampleInputPassword1"
                      className="form-label">
                      Password
                    </label>
                    <div className="input-group mb-3">
                      <input
                        type={this.state.show ? 'text' : 'password'}
                        className="form-control"
                        id="exampleInputPassword1"
                        onChange={(event) => {
                          this.setState({
                            password: event.target.value
                          });
                        }}
                      />
                      <span
                        className=" input-group-text"
                        id="basic-addon1"
                        onClick={() =>
                          this.setState({ show: !this.state.show })
                        }>
                        {this.state.show ? (
                          <VisibilityIcon />
                        ) : (
                          <VisibilityOffIcon />
                        )}
                      </span>
                    </div>
                  </div>
                  <div className="form-text text-end mb-3">
                    <Link href="/">Forgot password</Link>
                  </div>
                  <div className="text-center mb-3">
                    <button
                      type="Submit"
                      className="btn btn-primary"
                      onClick={this.submit.bind(this)}>
                      Submit
                    </button>
                  </div>
                  <div className="text-center form-text">
                    Don't have account ? <Link href="/register">Sign up</Link>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <ToastContainer />
        </main>
      </div>
    );
  }
}

export default index;
