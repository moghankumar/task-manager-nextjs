import Head from "next/head";
import Link from "next/link";
import React, { Component } from "react";
import styles from "../styles/Home.module.css";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export class forgetpassword extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
    };
  }
  forget(event) {
    event.preventDefault();
    if (this.state.email === "") {
      toast.error("Email must not be empty", {
        position: toast.POSITION.BOTTOM_RIGHT,
        autoClose: false,
      });
    } else if (!this.state.email.includes("@gmail.com")) {
      toast.error("Invalid Email type", {
        position: toast.POSITION.BOTTOM_RIGHT,
        autoClose: false,
      });
    } else {
      toast.success("You will get verify link in gmail please check", {
        position: toast.POSITION.BOTTOM_RIGHT,
        autoClose: false,
      });
    }
  }

  render() {
    return (
      <div className="container">
        <Head>
          <title>Forget Password</title>
          <meta name="description" content="Forget Password" />
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <main className={styles.centerscreen}>
          <div id={styles.main} className="row">
            <div className="col-lg-6 col-md-6">
              <h3 className=" text-color">Forget Password</h3>
            </div>
            <div className="col-lg-6 col-md-6">
              <div className={styles.card}>
                <form onSubmit={this.forget.bind(this)}>
                  <div className="mb-3">
                    <label htmlFor="exampleInputEmail1" className="form-label">
                      Email address
                    </label>
                    <input
                      type="email"
                      className="form-control"
                      id="exampleInputEmail1"
                      aria-describedby="emailHelp"
                      onChange={(event) => {
                        this.setState({ email: event.target.value });
                      }}
                    />
                  </div>

                  <div className="text-center mb-3">
                    <button
                      type="Submit"
                      className="btn btn-primary"
                      onClick={this.forget.bind(this)}
                    >
                      Submit
                    </button>
                  </div>
                  <div className="text-center form-text">
                    Don't have account ? <Link href="/register">Sign up</Link>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <ToastContainer />
        </main>
      </div>
    );
  }
}

export default forgetpassword;
