import Head from 'next/head';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';

import styles from '../styles/Home.module.css';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import Link from 'next/link';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { userDetail } from '../src/services/user-services';
import { updatePassword } from '../src/services/profile-services';
import { getCookie } from '../src/utils/cookie-helper';

function profile() {
  const router = useRouter();
  const [showcurrent, setShowcurrent] = useState(false);
  const [shownew, setShownew] = useState(false);
  const [showconfirm, setShowconfirm] = useState(false);
  const [currentpassword, setCurrentPassword] = useState('');
  const [newpassword, setNewPassword] = useState('');
  const [confirmpassword, setConfirmPassword] = useState('');
  const [user, setUser] = useState('');
  useEffect(() => {
    usertask();
  }, []);
  const usertask = async () => {
    const id = getCookie().userid;

    const { data, status, error } = await userDetail(id);
    if (error) {
      toast.error('Unable to get user details', {
        position: toast.POSITION.BOTTOM_RIGHT,
        autoClose: false
      });
    } else if (status === 200 && data) {
      setUser(data.data);
    }
  };
  const changepassword = async () => {
    const passworddata = {
      currentPassword: currentpassword,
      password: newpassword,
      confirmpassword: confirmpassword
    };
    const id = user.id;
    console.log('id check:', user.id);
    const { data, status, error } = await updatePassword(id, passworddata);
    console.log('Check:', data);
    if (error) {
      toast.error('Unable to change the password', {
        position: toast.POSITION.BOTTOM_RIGHT,
        autoClose: false
      });
    } else if (status === 200 && data) {
      console.log('Change PAssword:', data);
      toast.success('Password changed ', {
        position: toast.POSITION.BOTTOM_RIGHT,
        autoClose: false
      });
    }
  };
  function changePass(event) {
    event.preventDefault();

    if ((currentpassword && newpassword && confirmpassword) === '') {
      toast.warning('Password must not be in empty', {
        position: toast.POSITION.BOTTOM_RIGHT,
        autoClose: false
      });
    } else if (newpassword !== confirmpassword) {
      toast.warning('New Password and confirm password must be same', {
        position: toast.POSITION.BOTTOM_RIGHT,
        autoClose: false
      });
    } else if (newpassword === confirmpassword) {
      if (confirmpassword) {
        const re = new RegExp(
          '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})'
        );
        const check = re.test(confirmpassword);
        if (check === false) {
          toast.warning(
            'Password must be Minimum 8 letters and Uppercase, Lowercase, Special Character, Numeric ',
            {
              position: toast.POSITION.BOTTOM_RIGHT,
              autoClose: false
            }
          );
        } else {
          changepassword();
        }
      }
    }
  }

  return (
    <div className="container">
      <Head>
        <title>Profile</title>
        <meta name="description" content="Profile" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className={styles.centerscreen}>
        <div id={styles.main} className="row">
          <div className="col-lg-6 col-md-6">
            <div className={styles.card}>
              <h2 className="text-center">Profile</h2>
              <form>
                <div className="mb-3">
                  <label className="form-label">User name</label>
                  <input
                    type="text"
                    className="form-control"
                    value={user.name}
                    disabled="disabled"
                  />
                </div>
                <div className="mb-3">
                  <label htmlFor="exampleInputEmail1" className="form-label">
                    Email address
                  </label>
                  <input
                    type="email"
                    className="form-control"
                    id="exampleInputEmail1"
                    aria-describedby="emailHelp"
                    value={user.email}
                    disabled="disabled"
                  />
                  <div id="emailHelp" className="form-text">
                    We'll never share your email with anyone else.
                  </div>
                </div>

                <div
                  id="passwordHelp"
                  className="form-text text-right mb-3"
                  data-bs-toggle="modal"
                  data-bs-target="#staticBackdrop"
                  style={{ cursor: 'pointer' }}>
                  Change Password
                </div>

                <div className="text-center">
                  <button
                    type="submit"
                    className="btn btn-primary"
                    onClick={() => router.push('/dashboard')}>
                    Back
                  </button>
                </div>
              </form>
              <div className="text-center mt-3 form-text">
                Click to go back to <Link href="/dashboard">Dashboard</Link>
                <br />
                Click to go to <Link href="/">Login</Link>
              </div>
            </div>
          </div>
        </div>
        <div
          className="modal fade text-left"
          id="staticBackdrop"
          data-bs-backdrop="static"
          data-bs-keyboard="false"
          tabIndex="-1"
          aria-labelledby="staticBackdropLabel"
          aria-hidden="true">
          <div className="modal-dialog modal-dialog-centered">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  Change Password
                </h5>
                <button
                  type="button"
                  className="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"></button>
              </div>
              <div className="modal-body">
                <label htmlFor="exampleInputPassword1" className="form-label">
                  Current Password
                </label>
                <div className="input-group mb-3">
                  <input
                    type={showcurrent ? 'text' : 'password'}
                    className="form-control"
                    id="exampleInputPassword1"
                    onChange={(event) => setCurrentPassword(event.target.value)}
                  />
                  <span
                    className=" input-group-text"
                    id="basic-addon1"
                    onClick={() => setShowcurrent(!showcurrent)}>
                    {showcurrent ? <VisibilityIcon /> : <VisibilityOffIcon />}
                  </span>
                </div>
                <label htmlFor="exampleInputPassword2" className="form-label">
                  New password
                </label>
                <div className="input-group mb-3">
                  <input
                    type={shownew ? 'text' : 'password'}
                    className="form-control"
                    id="exampleInputPassword2"
                    onChange={(event) => {
                      setNewPassword(event.target.value);
                    }}
                  />
                  <span
                    className=" input-group-text"
                    id="basic-addon1"
                    onClick={() => setShownew(!shownew)}>
                    {shownew ? <VisibilityIcon /> : <VisibilityOffIcon />}
                  </span>
                </div>
                <div id="passwordHelpBlock" className="form-text">
                  Password must be 8-20 characters long, contain Uppercase
                  characters, Lowercase characters, special characters and
                  numbers, and must not contain emoji.
                </div>
                <label htmlFor="exampleInputPassword3" className="form-label">
                  Confirm Password
                </label>
                <div className="input-group mb-3">
                  <input
                    type={showconfirm ? 'text' : 'password'}
                    className="form-control"
                    id="exampleInputPassword3"
                    onChange={(event) => {
                      setConfirmPassword(event.target.value);
                    }}
                  />
                  <span
                    className=" input-group-text"
                    id="basic-addon1"
                    onClick={() => setShowconfirm(!showconfirm)}>
                    {showconfirm ? <VisibilityIcon /> : <VisibilityOffIcon />}
                  </span>
                </div>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-primary"
                  data-bs-dismiss="modal"
                  onClick={changePass}>
                  Save
                </button>
              </div>
            </div>
          </div>
        </div>
      </main>
      <ToastContainer />
    </div>
  );
}

export default profile;
