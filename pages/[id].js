import { withRouter } from "next/router";
import React from "react";
import styles from "../styles/Home.module.css";

export default withRouter(
  class Id extends React.Component {
    render() {
      return (
        <div className={styles.main}>
          <h3>{this.props.router.query.id}</h3>
        </div>
      );
    }
  }
);
