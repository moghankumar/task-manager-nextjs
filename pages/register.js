import Head from 'next/head';
import React, { Component } from 'react';
import Link from 'next/link';
import styles from '../styles/Home.module.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Router from 'next/router';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import { signup } from '../src/services/auth-services';
import { saveCookie } from '../src/utils/cookie-helper';

export class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      email: '',
      password: '',
      confirmpassword: '',
      checkpassword: '',
      checked: false,
      loading: false,
      showp: false,
      showc: false
    };
    this.submit = this.submit.bind(this);
  }
  submit(event) {
    event.preventDefault();
    if (this.state.username === '') {
      toast.warning('Username must needed', {
        position: toast.POSITION.BOTTOM_RIGHT,
        autoClose: false
      });
    } else if (this.state.password && this.state.confirmpassword) {
      const re = new RegExp(
        '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})'
      );
      const check = re.test(this.state.password);
      if (check === false) {
        toast.warning(
          'Password must be Minimum 8 letters and Uppercase, Lowercase, Special Character, Numeric ',
          {
            position: toast.POSITION.BOTTOM_RIGHT,
            autoClose: false
          }
        );
      } else {
        this.setState({ checkpassword: this.state.password }, () => {
          if (
            this.state.confirmpassword === this.state.checkpassword &&
            this.state.username &&
            this.state.email &&
            this.state.email.includes('@gmail.com')
          ) {
            this.setState({ checked: true });
            this.register();
            // Router.push({
            //   pathname: "/dashboard",
            //   query: { email: `${this.state.email}` },
            // });
          }
        });
      }
    } else if (this.state.email === '') {
      toast.error('Email id must needed', {
        position: toast.POSITION.BOTTOM_RIGHT,
        autoClose: false
      });
    } else if (!this.state.email.includes('@gmail.com')) {
      toast.error('Invalid Email type', {
        position: toast.POSITION.BOTTOM_RIGHT,
        autoClose: false
      });
    } else if (this.state.confirmpassword !== this.state.password) {
      toast.error('Password and confirm password must be same', {
        position: toast.POSITION.BOTTOM_RIGHT,
        autoClose: false
      });
    } else if (
      this.state.confirmpassword === '' &&
      this.state.password === ''
    ) {
      toast.error('Password and Confirm password must have valid information', {
        position: toast.POSITION.BOTTOM_RIGHT,
        autoClose: false
      });
    }
  }

  register = async () => {
    const userData = {
      name:
        this.state.username.charAt(0).toUpperCase() +
        this.state.username.slice(1),
      email: this.state.email,
      password: this.state.password,
      confirmPassword: this.state.confirmPassword
    };
    const { data, status, error } = await signup(userData);

    if (error) {
      toast.error('Failed...Check again', {
        position: toast.POSITION.BOTTOM_RIGHT,
        autoClose: false
      });
    } else if (status === 200 && data) {
      saveCookie('userid', data.user.id);
      Router.push('/dashboard');
    }
  };

  render() {
    return (
      <div className="container">
        <Head>
          <title>Registration</title>
          <meta name="description" content="Sign up your account" />
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <main className={styles.centerscreen}>
          <div id={styles.main} className="row">
            <div className="col-lg-6 col-md-6">
              <h3 className={styles.text_color}>Sign Up for free now...</h3>
            </div>
            <div className="col-lg-6 col-md-6">
              <div className={styles.card}>
                <h2 className="text-center">Registration</h2>
                <form onSubmit={this.submit.bind(this)}>
                  <div className="mb-3">
                    <label className="form-label">User name</label>
                    <input
                      type="text"
                      className="form-control"
                      onChange={(event) =>
                        this.setState({ username: event.target.value })
                      }
                    />
                  </div>
                  <div className="mb-3">
                    <label htmlFor="exampleInputEmail1" className="form-label">
                      Email address
                    </label>
                    <input
                      type="email"
                      className="form-control"
                      id="exampleInputEmail1"
                      aria-describedby="emailHelp"
                      onChange={(event) =>
                        this.setState({ email: event.target.value })
                      }
                    />
                    <div id="emailHelp" className="form-text">
                      We'll never share your email with anyone else.
                    </div>
                  </div>
                  <div className="mb-3">
                    <label
                      htmlFor="exampleInputPassword1"
                      className="form-label">
                      Password
                    </label>
                    <div className="input-group mb-3">
                      <input
                        type={this.state.showp ? 'text' : 'password'}
                        className="form-control"
                        id="exampleInputPassword1"
                        onChange={(event) => {
                          this.setState({ password: event.target.value });
                        }}
                      />
                      <span
                        className=" input-group-text"
                        id="basic-addon1"
                        onClick={() =>
                          this.setState({ showp: !this.state.showp })
                        }>
                        {this.state.showp ? (
                          <VisibilityIcon />
                        ) : (
                          <VisibilityOffIcon />
                        )}
                      </span>
                    </div>
                    <div id="passwordHelpBlock" className="form-text">
                      Password must be 8-20 characters long, contain Uppercase
                      characters, Lowercase characters, special characters and
                      numbers, and must not contain emoji.
                    </div>
                  </div>
                  <div className="mb-3">
                    <label
                      htmlFor="exampleInputPassword2"
                      className="form-label">
                      Confirm Password
                    </label>
                    <div className="input-group mb-3">
                      <input
                        type={this.state.showc ? 'text' : 'password'}
                        className="form-control"
                        id="exampleInputPassword2"
                        onChange={(event) => {
                          this.setState({
                            confirmpassword: event.target.value
                          });
                        }}
                      />
                      <span
                        className=" input-group-text"
                        id="basic-addon1"
                        onClick={() =>
                          this.setState({ showc: !this.state.showc })
                        }>
                        {this.state.showc ? (
                          <VisibilityIcon />
                        ) : (
                          <VisibilityOffIcon />
                        )}
                      </span>
                    </div>
                  </div>
                  <div className="text-center">
                    <button
                      type="submit"
                      className="btn btn-primary"
                      onClick={() => {
                        this.setState({ loading: true }),
                          this.submit.bind(this);
                      }}>
                      {/* {this.state.loading === true ? (
                        <span
                          class="spinner-border spinner-border-sm"
                          role="status"
                          aria-hidden="true"
                        ></span>
                      ) : null} */}
                      Submit
                    </button>
                  </div>
                  <div className="text-center mt-3 form-text">
                    Click to go back to <Link href="/">Login</Link>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <ToastContainer />
        </main>
      </div>
    );
  }
}

export default Register;
