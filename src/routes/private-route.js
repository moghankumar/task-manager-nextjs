import Router from 'next/router';

const PrivateRoute = (Component) => {
  return (props) => {
    //const Router = useRouter();
    if (typeof window !== 'undefined') {
      if (true) {
        Router.push('/register');
        return null;
      }
    }
    return <Component {...props} />;
  };
};

export default PrivateRoute;
