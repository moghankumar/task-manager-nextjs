import React from "react";
import { Bar } from "react-chartjs-2";

function Graphcontent() {
  return (
    <div>
      <h3 className="text-color">Graphical</h3>

      <Bar
        className="mt-3"
        height={400}
        data={{
          labels: [
            "No of projects",
            "Projects in progress",
            "Finished projects",
          ],
          datasets: [
            {
              label: "Project Details",
              data: [3, 1, 2],
              backgroundColor: ["#34495e", "#2980b9", "#3498db"],
              borderColor: ["#34495e", "#2980b9", "#3498db"],
              borderWidth: 1,
            },
          ],
        }}
        options={{
          maintainAspectRatio: false,
          scales: {
            yAxes: [
              {
                beginAtZero: true,
              },
            ],
          },
        }}
      />
    </div>
  );
}

export default Graphcontent;
