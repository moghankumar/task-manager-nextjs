import React from "react";
import Dashboardcontent from "./Dashboardcontent";
import Finishedcontent from "./Finishedcontent";
import Inprogresscontent from "./Inprogresscontent";
import Taskcontent from "./Taskcontent";

function Dashboard(props) {
  return (
    <div>
      {props.count === 0 ? (
        <Dashboardcontent />
      ) : props.count === 1 ? (
        <Taskcontent />
      ) : props.count === 2 ? (
        <Inprogresscontent />
      ) : props.count == 3 ? (
        <Finishedcontent />
      ) : (
        <h3>Please Choose correct options</h3>
      )}
    </div>
  );
}

export default Dashboard;
