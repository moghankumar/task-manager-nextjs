import React, { Component } from 'react';
import Avatar from 'react-avatar';

export class Inprogresscontent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      press: '',
      show: false
    };
  }

  render() {
    const inprogress = [
      {
        title: 'Front End Development',
        des: 'UI working',
        working: 'Moghan Kumar',
        projectname: 'Task Manager'
      },
      {
        title: 'Back End Development',
        des: 'API',
        working: 'Santhosh',
        projectname: 'Task Manager'
      }
    ];
    return (
      <div>
        <h3 className="text-color">In Progress</h3>
        <div className="mt-3 row"></div>
        <div className="mt-3 table-responsive">
          <table className="table align-middle table-striped">
            <thead className="table-light">
              <tr>
                <th scope="col">Task name</th>
                <th scope="col">Description</th>
                <th scope="col">Project</th>
                <th scope="col">Working by </th>
              </tr>
            </thead>
            <tbody>
              {inprogress.map((i, index) => {
                return (
                  <tr scope="row" key={index}>
                    <td>{i.title}</td>
                    <td>{i.des}</td>
                    <td>{i.projectname}</td>
                    <td>
                      <Avatar
                        name={i.working}
                        size="35"
                        color={'#0070f3'}
                        textSizeRatio={1.75}
                        round={true}
                      />
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default Inprogresscontent;
