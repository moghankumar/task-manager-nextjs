import React, { Component } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { createproject, showproject } from '../services/dashboard-services';

export class Dashboardcontent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      projectname: '',
      projectdesc: '',
      projectstart: '',
      projectend: '',
      projectworks: '',
      projectstatus: 'Active',
      projectdata: '',
      user: ''
    };
    this.showprojects = this.showprojects.bind(this);
  }

  componentDidMount() {
    this.showprojects();
  }

  Add(event) {
    event.preventDefault();
    if (
      this.state.projectname === '' &&
      this.state.projectdesc === '' &&
      this.state.projectstart === '' &&
      this.state.projectend === '' &&
      this.state.projectworks === ''
    ) {
      toast.warning('Project Details must needed', {
        position: toast.POSITION.BOTTOM_RIGHT,
        autoClose: false
      });
    } else {
      this.project();
    }
  }

  project = async () => {
    const projectdata = {
      projectName: this.state.projectname,
      projectDescription: this.state.projectdesc,
      startDate: this.state.projectstart,
      endDate: this.state.projectend,
      status: this.state.projectstatus,
      developer: this.state.projectworks
    };
    const { data, status, error } = await createproject(projectdata);
    if (error) {
      toast.error('Unable to create project', {
        position: toast.POSITION.BOTTOM_RIGHT,
        autoClose: false
      });
    } else if (status === 201 && data) {
      this.showprojects();
    }
  };
  showprojects = async () => {
    const { data, status, error } = await showproject();
    if (error) {
      toast.error('Unable to show project list', {
        position: toast.POSITION.BOTTOM_RIGHT,
        autoClose: false
      });
    } else if (status === 200 && data) {
      this.setState({ projectdata: data.data });
    }
  };

  render() {
    const card = [
      {
        title: 'No of Projects',
        value: this.state.projectdata.length + 2
      },
      {
        title: 'No of Projects in progress',
        value: this.state.projectdata.length
      },
      { title: 'No of Projects finished', value: 2 }
    ];
    return (
      <div>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <h3 className="text-color">Dashboard</h3>
          <form className="d-flex ">
            <input
              className="form-control me-2 "
              type="search"
              placeholder="Search"
              aria-label="Search"
            />
            <button
              className="btn btn-outline-primary bg-white text-color"
              type="submit">
              Search
            </button>
          </form>
        </div>
        <div className="row gx-3 gy-3 mt-3">
          {card.map((i, index) => {
            return (
              <div className=" col-lg-3 col-md-3 " key={index}>
                <div className="card">
                  <div className="card-body">
                    <h5 className="card-title text-color">{i.title}</h5>
                    <p className="card-text">{i.value}</p>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
        <div className="table-responsive">
          <table className="table mt-3  align-middle table-striped">
            <thead className="table-light">
              <tr>
                <th scope="col"></th>
                <th scope="col">Project name</th>
                <th scope="col">Project Description</th>
                <th scope="col">Project handover (started) </th>
                <th scope="col">Estimated time</th>
                <th scope="col">Given to</th>
                <th scope="col">Status</th>
              </tr>
              <tr>
                <th scope="col"></th>
                <th scope="col">
                  <input
                    type="text"
                    className="form-control border border-primary "
                    placeholder="Project name"
                    onChange={(event) => {
                      this.setState({ projectname: event.target.value });
                    }}
                  />
                </th>
                <th scope="col">
                  <input
                    type="text"
                    className="form-control border border-primary"
                    placeholder="Project description"
                    onChange={(event) => {
                      this.setState({ projectdesc: event.target.value });
                    }}
                  />
                </th>
                <th scope="col">
                  <input
                    type="date"
                    className="form-control border border-primary"
                    placeholder="Project handover"
                    onChange={(event) => {
                      this.setState({ projectstart: event.target.value });
                    }}
                  />
                </th>
                <th scope="col">
                  <input
                    type="date"
                    className="form-control border border-primary"
                    placeholder="Estimated time"
                    onChange={(event) => {
                      this.setState({ projectend: event.target.value });
                    }}
                  />
                </th>
                <th scope="col">
                  <input
                    type="text"
                    className="form-control border border-primary"
                    placeholder="Given to"
                    onChange={(event) => {
                      this.setState({ projectworks: event.target.value });
                    }}
                  />
                </th>
                <th scope="col">
                  <button
                    type="submit"
                    className="btn btn-outline-primary"
                    onClick={this.Add.bind(this)}>
                    Add
                  </button>
                </th>
              </tr>
            </thead>
            <tbody>
              {this.state.projectdata &&
                this.state.projectdata.map((i, index) => {
                  return (
                    <tr scope="row" key={index}>
                      <td>{index + 1}</td>
                      <th>{i.projectName}</th>
                      <td>{i.projectDescription}</td>
                      <td>{i.startDate}</td>
                      <td>{i.endDate}</td>
                      <td>{i.developer}</td>
                      <td>{i.status}</td>
                    </tr>
                  );
                })}
            </tbody>
          </table>
        </div>

        <ToastContainer />
      </div>
    );
  }
}

export default Dashboardcontent;
