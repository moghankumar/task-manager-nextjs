import React, { Component } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {
  createTask,
  showTask,
  updateTask,
  deleteTask
} from '../services/task-services';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import { Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import Kanbanboard from './Kanbanboard';

export class Taskcontent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      show: false,
      tasktitle: '',
      projectname: '',
      projectcategory: '',
      showdata: '',
      id: ''
    };
  }
  componentDidMount() {
    this.showtask();
  }
  project(event) {
    event.preventDefault();
    if (this.state.projectname === '' && this.state.tasktitle === '') {
      toast.error('Task name and project name must be needed', {
        position: toast.POSITION.BOTTOM_RIGHT
      });
    } else {
      this.createtask();
    }
  }
  createtask = async () => {
    const task = {
      taskName:
        this.state.tasktitle.charAt(0).toUpperCase() +
        this.state.tasktitle.slice(1),
      projectName:
        this.state.projectname.charAt(0).toUpperCase() +
        this.state.projectname.slice(1),
      status: 'true'
    };
    const { data, status, error } = await createTask(task);

    if (error) {
      toast.error('Unable to create task', {
        position: toast.POSITION.BOTTOM_RIGHT,
        autoClose: false
      });
    } else if (status === 201 && data) {
      this.showtask();
    }
  };
  showtask = async () => {
    const { data, status, error } = await showTask();
    if (error) {
      toast.error('Unable to show the task details try again', {
        position: toast.POSITION.BOTTOM_RIGHT,
        autoClose: false
      });
    } else if (status === 200 && data) {
      this.setState({ showdata: data.data });
    }
  };
  update(id, event) {
    event.preventDefault();
    if (this.state.projectname === '' && this.state.tasktitle === '') {
      toast.error('While updating, All fields must have values', {
        position: toast.POSITION.BOTTOM_RIGHT
      });
    } else {
      this.updatetask(id);
    }
  }
  updatetask = async (id) => {
    const updateData = {
      taskName: this.state.tasktitle,
      projectName: this.state.projectname,
      status: 'true'
    };
    const { data, status, error } = await updateTask(id, updateData);
    if (error) {
      toast.error('Unable to update the task', {
        position: toast.POSITION.BOTTOM_RIGHT,
        autoClose: false
      });
    } else if (status === 201 && data) {
      this.showtask();
    }
  };
  deleteTask = async (id) => {
    const { data, status, error } = await deleteTask(id);
    if (error) {
      toast.error('Unable to delete the task', {
        position: toast.POSITION.BOTTOM_RIGHT,
        autoClose: false
      });
    } else if (status === 200 && data) {
      this.showtask();
    }
  };
  render() {
    return (
      <div>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <h3 className="text-color">Task</h3>
          <button
            type="submit"
            className="btn btn-outline-primary"
            data-bs-toggle="modal"
            data-bs-target="#staticBackdrop"
            onClick={() => this.setState({ show: !this.state.show })}>
            Add new task
          </button>
        </div>
        <div className="mt-3 table-responsive">
          <table className="table align-middle table-striped ">
            <thead className="table-light">
              <tr>
                <th scope="col">Task name</th>
                <th scope="col">Project Name</th>
                <th scope="col">Status</th>
              </tr>
            </thead>
            <tbody>
              {this.state.showdata ? (
                <>
                  {this.state.showdata.map((i, index) => {
                    return (
                      <tr scope="row" key={index}>
                        <td>{i.taskName}</td>
                        <td> {i.projectName}</td>
                        <td>
                          <OverlayTrigger
                            key="left"
                            placement="left"
                            overlay={
                              <Tooltip id="tooltip-left">
                                <strong>Update</strong>.
                              </Tooltip>
                            }>
                            <Button
                              variant="outline-secondary"
                              type="button"
                              data-bs-toggle="modal"
                              data-bs-target="#staticBackdrop2"
                              onClick={() => this.setState({ id: i._id })}>
                              <EditIcon />
                            </Button>
                          </OverlayTrigger>
                          {'   '}
                          <OverlayTrigger
                            key="right"
                            placement="right"
                            overlay={
                              <Tooltip id="tooltip-right">
                                <strong>Delete</strong>.
                              </Tooltip>
                            }>
                            <Button
                              variant="outline-danger"
                              type="button"
                              onClick={this.deleteTask.bind(this, i._id)}>
                              <DeleteIcon />
                            </Button>
                          </OverlayTrigger>
                        </td>
                      </tr>
                    );
                  })}
                </>
              ) : (
                <tr>
                  <td colSpan="100%" className="text-center">
                    No data
                  </td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
        <Kanbanboard />

        <ToastContainer />

        {/* Add new Task */}
        <div
          className="modal fade"
          id="staticBackdrop"
          data-bs-backdrop="static"
          data-bs-keyboard="false"
          tabindex="-1"
          aria-labelledby="staticBackdropLabel"
          aria-hidden="true">
          <div className="modal-dialog modal-dialog-centered">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  New task
                </h5>
                <button
                  type="button"
                  className="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"></button>
              </div>{' '}
              <form onSubmit={this.project.bind(this)}>
                <div className="modal-body">
                  <label className="form-label">Task Name</label>
                  <input
                    className="form-control"
                    type="text"
                    placeholder="Task name"
                    onChange={(event) => {
                      this.setState({ tasktitle: event.target.value });
                    }}
                  />
                  <label className="form-label">Project Name</label>
                  <input
                    className="form-control"
                    type="text"
                    placeholder="Project name"
                    onChange={(event) => {
                      this.setState({ projectname: event.target.value });
                    }}
                  />
                </div>
                <div className="modal-footer">
                  <button
                    className="btn btn-primary"
                    data-bs-dismiss="modal"
                    onClick={this.project.bind(this)}>
                    Add
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
        {/* Updating task */}
        <div
          className="modal fade"
          id="staticBackdrop2"
          data-bs-backdrop="static"
          data-bs-keyboard="false"
          tabindex="-1"
          aria-labelledby="staticBackdropLabel"
          aria-hidden="true">
          <div className="modal-dialog modal-dialog-centered">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  Updating the task
                </h5>
                <button
                  type="button"
                  className="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"></button>
              </div>
              <form onSubmit={this.update.bind(this, this.state.id)}>
                <div className="modal-body">
                  <label className="form-label">Task Name</label>
                  <input
                    className="form-control"
                    type="text"
                    placeholder="Task name"
                    onChange={(event) => {
                      this.setState({ tasktitle: event.target.value });
                    }}
                  />
                  <label className="form-label">Project Name</label>
                  <input
                    className="form-control"
                    type="text"
                    placeholder="Project name"
                    onChange={(event) => {
                      this.setState({ projectname: event.target.value });
                    }}
                  />
                </div>
                <div className="modal-footer">
                  <button
                    className="btn btn-primary"
                    data-bs-dismiss="modal"
                    onClick={this.update.bind(this, this.state.id)}>
                    Add
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Taskcontent;
