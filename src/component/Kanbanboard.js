import React, { useState, useEffect } from 'react';
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd';
import _ from 'lodash';
import { showTask } from '../services/task-services';

const dataitems = [
  {
    title: 'Front End Development',
    des: 'UI working',
    id: '3a'
  },
  {
    title: 'Back End Development',
    des: 'API',
    id: '3b'
  }
];

function Kanbanboard() {
  const [datatask, setDatatask] = useState('');

  const [column, setColumn] = useState({
    todo: {
      title: 'To do',
      items: dataitems
    },

    inprogress: {
      title: 'In Progress',
      items: []
    },
    // inreview: {
    //   title: 'In Review',
    //   items: []
    // },
    finished: {
      title: 'Finished',
      items: []
    }
  });

  const columndata = {
    todo: {
      title: 'To do',
      items: datatask
    },

    inprogress: {
      title: 'In Progress',
      items: []
    },
    // inreview: {
    //   title: 'In Review',
    //   items: []
    // },
    finished: {
      title: 'Finished',
      items: []
    }
  };

  useEffect(() => {
    showtask();
  }, []);
  const showtask = async () => {
    const { data, status, error } = await showTask();
    if (error) {
      toast.error('Unable to show the task details try again', {
        position: toast.POSITION.BOTTOM_RIGHT,
        autoClose: false
      });
    } else if (status === 200 && data) {
      setDatatask(data.data);
    }
  };
  const handleDrag = ({ source, destination }) => {
    console.log('From:', source);
    console.log('To : ', destination);

    if (!destination) {
      return;
    }
    if (
      destination.index === source.index &&
      destination.droppableId === source.droppableId
    ) {
      return;
    }

    const itemcopy = {
      ...column[source.droppableId].items[source.index]
    };

    setColumn((prev) => {
      prev = { ...prev };
      prev[source.droppableId].items.splice(source.index, 1);

      prev[destination.droppableId].items.splice(
        destination.index,
        0,
        itemcopy
      );
      return prev;
    });
  };
  // const handleDrag = (result) => {
  //   console.log('Result :', result);
  //   if (!result.destination) return;
  //   const { source, destination } = result;
  //   if (source.droppableId !== destination.droppableId) {
  //     const sourceColumn = column[source.droppableId];
  //     const destinationColumn = column[destination.droppableId];
  //     const sourceItems = [...sourceColumn.items];
  //     const destinationItems = [...destinationColumn.items];
  //     const [removed] = sourceItems.splice(source.index, 1);
  //     destinationItems.splice(destination.index, 0, removed);

  //     setColumn({
  //       ...column,
  //       [source.droppableId]: {
  //         ...sourceColumn,
  //         items: sourceItems
  //       },
  //       [destination.droppableId]: {
  //         ...destinationColumn,
  //         items: destinationItems
  //       }
  //     });
  //   } else {
  // const copieditems = {
  //   ...column[source.droppableId].items
  // };
  //     const [removed] = copieditems.splice(source.index, 1);
  //     copieditems.splice(destination.index, o, removed);
  //     setColumn({
  //       ...column,
  //       [source.draggableId]: {
  //         ...column,
  //         items: copieditems
  //       }
  //     });
  //   }
  // };
  return (
    <div>
      <div className="row">
        <DragDropContext onDragEnd={handleDrag}>
          {_.map(column, (data, key) => {
            return (
              <div className="col-4 column">
                <h5 className="text-center">{data.title}</h5>
                <Droppable droppableId={key}>
                  {(provided, snapshot) => {
                    return (
                      <div
                        ref={provided.innerRef}
                        {...provided.droppableProps}
                        className="droppable-col">
                        {data.items.map((t, indexmain) => {
                          return (
                            <Draggable
                              key={t.id}
                              index={indexmain}
                              draggableId={t.id}>
                              {(provided, snapshot) => {
                                return (
                                  <div
                                    className="draggable-item"
                                    ref={provided.innerRef}
                                    {...provided.draggableProps}
                                    {...provided.dragHandleProps}>
                                    <h5>Task name: {t.title}</h5>

                                    <p>Task Description: {t.des}</p>
                                  </div>
                                );
                              }}
                            </Draggable>
                          );
                        })}{' '}
                        {provided.placeholder}
                      </div>
                    );
                  }}
                </Droppable>
              </div>
            );
          })}
        </DragDropContext>
      </div>
    </div>
  );
}

export default Kanbanboard;
