import React, { Component } from "react";

export class Finishedcontent extends Component {
  render() {
    return (
      <div>
        <h3 className="text-color">Finished</h3>
        <div className="mt-3 row">
          <div className="col-lg-3 col-md-3">
            <div className="card">
              <div className="card-body">
                <h5 className="card-title text-color">Prism</h5>
                <p className="card-text">UI Working</p>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-md-3">
            <div className="card">
              <div className="card-body">
                <h5 className="card-title text-color">Prism</h5>
                <p className="card-text">Backend Working</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Finishedcontent;
