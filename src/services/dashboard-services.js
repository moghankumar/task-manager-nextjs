import { postRequest, getRequest } from '../utils/http-helper';
import { CREATE_PROJECT, SHOW_PROJECT } from '../constants/index';

export const createproject = async (data) => {
  return await postRequest({
    url: CREATE_PROJECT,
    data,
    noAuth: true
  });
};
export const showproject = async () => {
  return await getRequest({
    url: SHOW_PROJECT,
    noAuth: true
  });
};
