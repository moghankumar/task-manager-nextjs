import { getRequest } from '../utils/http-helper';
import { USER_DETAIL } from '../constants/index';

export const userDetail = async (id) => {
  return await getRequest({
    url: `${USER_DETAIL}/${id}`,
    noAuth: true
  });
};
