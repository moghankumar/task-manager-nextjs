import { postRequest } from '../utils/http-helper';
import { SIGNUP_URL, LOGIN_URL } from '../constants/index';

export const signup = async (data) => {
  return await postRequest({
    url: SIGNUP_URL,
    data
  });
};
export const login = async (data) => {
  return await postRequest({
    url: LOGIN_URL,
    data
  });
};
