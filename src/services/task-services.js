import {
  postRequest,
  getRequest,
  patchRequest,
  deleteRequest
} from '../utils/http-helper';
import {
  CREATE_TASK,
  SHOW_TASK,
  UPDATE_TASK,
  DELETE_TASK
} from '../constants/index';

export const createTask = async (data) => {
  return await postRequest({
    url: CREATE_TASK,
    data,
    noAuth: true
  });
};

export const showTask = async () => {
  return await getRequest({
    url: SHOW_TASK,
    noAuth: true
  });
};

export const updateTask = async (id, data) => {
  return await patchRequest({
    url: `${UPDATE_TASK}/${id}`,
    data,
    noAuth: true
  });
};

export const deleteTask = async (id) => {
  return await deleteRequest({
    url: `${DELETE_TASK}/${id}`,
    noAuth: true
  });
};
