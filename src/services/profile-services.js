import { postRequest } from '../utils/http-helper';
import { UPDATE_PASSWORD } from '../constants/index';

export const updatePassword = async (id, data) => {
  return await postRequest({
    url: `${UPDATE_PASSWORD}/${id}`,
    data,
    noAuth: true
  });
};
