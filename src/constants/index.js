export const AUTH_KEY = 'token';

export const BASE_URL = 'http://localhost:7070';

export const SIGNUP_URL = `${BASE_URL}/api/v1/auth/signup`;

export const LOGIN_URL = `${BASE_URL}/api/v1/auth/login`;

export const CREATE_PROJECT = `${BASE_URL}/api/v1/dashboard/createProject`;

export const SHOW_PROJECT = `${BASE_URL}/api/v1/dashboard/project`;

export const CREATE_TASK = `${BASE_URL}/api/v1/task/createTask`;

export const SHOW_TASK = `${BASE_URL}/api/v1/task/taskDetails`;

export const UPDATE_TASK = `${BASE_URL}/api/v1/task`;

export const DELETE_TASK = `${BASE_URL}/api/v1/task`;

export const USER_DETAIL = `${BASE_URL}/api/v1/user`;

export const UPDATE_PASSWORD = `${BASE_URL}/api/v1/user`;
